package roxana.tfm.frida;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import roxana.tfm.frida.cryptography.CryptographyActivity;
import roxana.tfm.frida.datastorage.DataStorageActivity;
import roxana.tfm.frida.network.NetworkActivity;
import roxana.tfm.frida.platformInteraction.PlatformInteractionActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void dataStorageTests(View view) {
        startActivity(new Intent(this, DataStorageActivity.class));
    }

    public void cryptographyTests(View view) {
        startActivity(new Intent(this, CryptographyActivity.class));
    }

    public void networkTests(View view) {
        startActivity(new Intent(this, NetworkActivity.class));
    }

    public void platformInteractionTests(View view) {
        startActivity(new Intent(this, PlatformInteractionActivity.class));
    }
}
