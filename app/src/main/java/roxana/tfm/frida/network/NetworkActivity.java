package roxana.tfm.frida.network;

import android.app.Activity;
import android.net.http.X509TrustManagerExtensions;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import roxana.tfm.frida.R;

public class NetworkActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);
    }

    static String urlStringGET = "https://reqres.in/api/users?page=2";
    static String urlStringPOST = "https://reqres.in/api/users";
    static String postBody = "{\n" +
            "    \"name\": \"alice\",\n" +
            "    \"job\": \"leader\"\n" +
            "}";

    public void openConnection(View view) {
        openConnectionAsyncTask();
    }

    private static void openConnectionAsyncTask() {
        (new AsyncTask<Void, Void, Void>() {
            HttpURLConnection connection = null;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlStringGET);
                    connection = (HttpURLConnection) url.openConnection();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    connection.disconnect();
                }
                return null;
            }
        }).execute();
    }

    public void newURL(View view) {
        try {
            new URL(urlStringGET);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void getContent(View view) {
        getContentAsyncTask();
    }

    private static void getContentAsyncTask() {
        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlStringGET);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.setDoOutput(false);
                    try {
                        connection.getContent();
                        connection.getContentType();
                        connection.getContentLength();
                        connection.getContentLengthLong();
                        connection.getContentEncoding();
                        connection.getDate();
                        connection.getExpiration();
                        connection.getLastModified();
                        connection.getResponseCode();
                    } finally {
                        connection.disconnect();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).execute();
    }

    public void readLine(View view) {
        createConnectionAndReadAsyncTask();
    }

    private static void createConnectionAndReadLineAsyncTask() {
        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlStringGET);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    try {
                        readStreamWithReadLineMethod(new BufferedInputStream(connection.getInputStream()));
                    } finally {
                        connection.disconnect();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).execute();
    }

    public void read(View view) {
        createConnectionAndReadAsyncTask();
    }

    private static void createConnectionAndReadAsyncTask() {
        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlStringGET);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    try {
                        readStreamWithReadMethod(new BufferedInputStream(connection.getInputStream()));
                    } finally {
                        connection.disconnect();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).execute();
    }

    public void write(View view) {
        sendPostRequestAsyncTask();
    }

    private static void sendPostRequestAsyncTask() {
        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlStringPOST);
                    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setDoOutput(true);
                    connection.connect();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(postBody);

                    writer.flush();
                    writer.close();
                    os.close();
                    int responseCode = connection.getResponseCode();
                    if (responseCode == HttpsURLConnection.HTTP_CREATED) {
                        readStreamWithReadLineMethod(new BufferedInputStream(connection.getInputStream()));
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).execute();
    }


    private static String readStreamWithReadLineMethod(InputStream in) throws IOException {
        String result = "";

        if (in != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = bufferedReader.readLine()) != null)
                result += line;
        }
        in.close();

        return result;
    }

    private static String readStreamWithReadMethod(InputStream in) throws IOException {
        String result = "";

        if (in != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            int value;

            while ((value = bufferedReader.read()) != -1) {
                result += (char) value;
            }
        }
        in.close();

        return result;
    }

    public void checkServerTrusted(View view) {
        connectToServerAndCheckSSLPinning();
    }

    private void connectToServerAndCheckSSLPinning() {

        TrustManagerFactory trustManagerFactory = null;
        try {
            trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            trustManagerFactory.init((KeyStore) null);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        // Find first X509TrustManager in the TrustManagerFactory
        X509TrustManager x509TrustManager = null;
        for (TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
            if (trustManager instanceof X509TrustManager) {
                x509TrustManager = (X509TrustManager) trustManager;
                break;
            }
        }

        X509TrustManagerExtensions trustManagerExt = new X509TrustManagerExtensions(x509TrustManager);
        Set<String> validPins = Collections.singleton("CZEvkurQ3diX6pndH4Z5/dUNzK1Gm6+n8Hdx/DQgjO0=");

        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlStringGET);
                    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    connection.connect();
                    if (validatePinning(trustManagerExt, connection, validPins)) {
                        runOnUiThread(() -> Toast.makeText(NetworkActivity.this, "Connected to a trusted server", Toast.LENGTH_SHORT).show());
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).execute();
    }

    private boolean validatePinning(X509TrustManagerExtensions trustManagerExt, HttpsURLConnection conn, Set<String> validPins) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            List<X509Certificate> trustedChain = trustedChain(trustManagerExt, conn);

            for (X509Certificate cert : trustedChain) {
                byte[] publicKey = cert.getPublicKey().getEncoded();
                md.update(publicKey, 0, publicKey.length);

                String pin = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                if (validPins.contains(pin))
                    return true;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    private List<X509Certificate> trustedChain(X509TrustManagerExtensions trustManagerExt, HttpsURLConnection conn) {
        Certificate[] serverCerts = new Certificate[0];
        try {
            serverCerts = conn.getServerCertificates();
        } catch (SSLPeerUnverifiedException e) {
            e.printStackTrace();
        }

        X509Certificate[] untrustedCerts = Arrays.copyOf(serverCerts,
                serverCerts.length, X509Certificate[].class);

        String host = conn.getURL().getHost();

        try {
            return trustManagerExt.checkServerTrusted(untrustedCerts, "RSA", host);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        return null;
    }
}
