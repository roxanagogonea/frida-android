package roxana.tfm.frida.datastorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.os.CancellationSignal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roxana on 15/3/18.
 */

public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "datastorage.db";

    // Contacts table name
    private static final String TABLE_USERS = "users";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PASSWORD = "password";

    public SQLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
            + KEY_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, " + KEY_NAME + " TEXT, "
            + KEY_PASSWORD + " TEXT" + ")";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS, new Object[]{});

        // Create tables again
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);
    }

    /*
     * Testing methods:
     * void execSQL(String sql)
     * void execSQL(String sql, Object[] bindArgs)
     */
    public void forceDatabaseCreation() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS, new Object[]{});

        // Create tables again
        db.execSQL(CREATE_USERS_TABLE);
    }

    /*
     * Testing methods:
     * long insert (String table, String nullColumnHack, ContentValues values)
     * long insertOrThrow (String table, String nullColumnHack, ContentValues values)
     * long insertWithOnConflict(String table, String nullColumnHack, ContentValues initialValues, int conflictAlgorithm)
     */
    public void insert() {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, "user1"); // Contact Name
        values.put(KEY_PASSWORD, "1234"); // Contact Phone Number

        // Inserting Row
        db.insert(TABLE_USERS, null, values);

        values = new ContentValues();
        values.put(KEY_NAME, "user2"); // Contact Name
        values.put(KEY_PASSWORD, "1111"); // Contact Phone Number

        // Inserting Row
        db.insertOrThrow(TABLE_USERS, null, values);


        values = new ContentValues();
        values.put(KEY_NAME, "user3"); // Contact Name
        values.put(KEY_PASSWORD, "password"); // Contact Phone Number

        // Inserting Row
        db.insertWithOnConflict(TABLE_USERS, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        db.close(); // Closing database connection
    }

    // Getting single user
    public User getUser(String name) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_USERS + " WHERE " + KEY_NAME + " = ?";

        String[] arguments = {name};

        Cursor c = db.rawQuery(selectQuery, arguments);

        if (c != null && c.moveToFirst()) {
            User user = new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD)));
            return user;
        }

        return null;
    }

    // Getting All Users
    public List<User> getAllUsers() {
        List<User> usersList = new ArrayList<User>();
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)),
                        c.getString(c.getColumnIndex(KEY_NAME)),
                        c.getString(c.getColumnIndex(KEY_PASSWORD))));
            } while (c.moveToNext());
        }

        return usersList;
    }

    public List<User> rawQueryWithFactory() {
        List<User> usersList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_USERS + " WHERE " + KEY_ID + "=?";

        SQLiteDatabase.CursorFactory cursorFactory = (sqLiteDatabase, sqLiteCursorDriver, s, sqLiteQuery) -> new SQLiteCursor(sqLiteDatabase, sqLiteCursorDriver, s, sqLiteQuery);

        Cursor c = db.rawQueryWithFactory(cursorFactory, selectQuery, new String[]{"2"}, null, new CancellationSignal());
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        c = db.rawQueryWithFactory(cursorFactory, selectQuery, new String[]{"1"}, null);
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        return usersList;
    }

    /*
     * Testing methods:
     * Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     * Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     * Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit, CancellationSignal cancellationSignal)
     * Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
     */
    public List<User> query() {
        List<User> usersList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        // query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
        Cursor c = db.query(true, TABLE_USERS, null, KEY_ID + "=?", new String[]{"1"}, null, null, null, null);
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        // query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
        c = db.query(TABLE_USERS, null, KEY_ID + "=?", new String[]{"2"}, null, null, null, null);
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        // query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit, CancellationSignal cancellationSignal)
        c = db.query(false, TABLE_USERS, null, KEY_ID + "=?", new String[]{"3"}, null, null, null, null, new CancellationSignal());
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        // query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
        c = db.query(TABLE_USERS, null, KEY_ID + "=?", new String[]{"4"}, null, null, null);
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        return usersList;
    }

    /*
     * Testing methods:
     * Cursor queryWithFactory(SQLiteDatabase.CursorFactory cursorFactory, boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit, CancellationSignal cancellationSignal)
     * Cursor queryWithFactory(SQLiteDatabase.CursorFactory cursorFactory, boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     */
    public List<User> queryWithFactory() {
        List<User> usersList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        SQLiteDatabase.CursorFactory cursorFactory = new SQLiteDatabase.CursorFactory() {
            @Override
            public Cursor newCursor(SQLiteDatabase sqLiteDatabase, SQLiteCursorDriver sqLiteCursorDriver, String s, SQLiteQuery sqLiteQuery) {
                return new SQLiteCursor(sqLiteDatabase, sqLiteCursorDriver, s, sqLiteQuery);
            }
        };

        Cursor c = db.queryWithFactory(cursorFactory, true, TABLE_USERS, null, KEY_ID + "=?", new String[]{"1"}, null, null, null, null);
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        c = db.queryWithFactory(cursorFactory, true, TABLE_USERS, null, KEY_ID + "=?", new String[]{"2"}, null, null, null, null, new CancellationSignal());
        if (c != null && c.moveToFirst()) {
            usersList.add(new User(c.getInt(c.getColumnIndex(KEY_ID)), c.getString(c.getColumnIndex(KEY_NAME)), c.getString(c.getColumnIndex(KEY_PASSWORD))));
        }

        return usersList;
    }

    /*
     * Testing methods:
     * update(String table, ContentValues values, String whereClause, String[] whereArgs)
     * updateWithOnConflict(String table, ContentValues values, String whereClause, String[] whereArgs, int conflictAlgorithm)
     */
    public void updateUser() {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PASSWORD, "12345678");

        // updating row
        db.update(TABLE_USERS, values, KEY_NAME + " = ?",
                new String[]{String.valueOf("user1")});


        values.put(KEY_NAME, "user5");
        db.updateWithOnConflict(TABLE_USERS, values, KEY_NAME + " = ?",
                new String[]{String.valueOf("user2")}, SQLiteDatabase.CONFLICT_ABORT);
    }
}
