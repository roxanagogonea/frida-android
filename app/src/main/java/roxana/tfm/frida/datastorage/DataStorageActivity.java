package roxana.tfm.frida.datastorage;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import roxana.tfm.frida.R;

/**
 * Created by roxana on 15/3/18.
 */

public class DataStorageActivity extends AppCompatActivity {
    private SQLiteDatabaseHelper sqLiteDatabaseHelper;

    private TextView inputTypeTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_storage);

        inputTypeTextView = findViewById(R.id.inputTypeTextView);

        sqLiteDatabaseHelper = new SQLiteDatabaseHelper(this);
    }

    // SharedPreferences

    public void setUpSharedPreferences(View view) {

        Log.i("TAG", "Called setUpSharedPreferences");
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefs", 0); // 0 for private mode
        SharedPreferences.Editor edit = pref.edit();
        addValuesToSharedPreferences(edit);

        pref.getString("STRING_VALUE", "");
    }

    private void addValuesToSharedPreferences(SharedPreferences.Editor editor) {
        editor.putString("STRING_VALUE", "Adding a String value to SharedPreferences object");
        editor.putBoolean("BOOLEAN_VALUE", true);
        editor.putFloat("FLOAT_VALUE", 10.5f);
        editor.putInt("INT_VALUE", 10);
        editor.putLong("LONG_VALUE", 100);

        Set<String> stringSet = new HashSet<>();
        stringSet.add("a");
        stringSet.add("b");
        stringSet.add("c");
        editor.putStringSet("STRING_SET_VALUE", stringSet);

        editor.commit();
    }

    // SQLite

    /*
     * Testing methods:
     * void execSQL(String sql)
     * void execSQL(String sql, Object[] bindArgs)
     */
    public void execSQL(View view) {
        sqLiteDatabaseHelper.forceDatabaseCreation();
    }

    /*
     * Testing methods:
     * Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     * Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     * Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit, CancellationSignal cancellationSignal)
     * Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
     */
    public void query(View view) {
        StringBuilder toastText = new StringBuilder();
        List<User> userList = sqLiteDatabaseHelper.query();

        for (User u : userList) {
            toastText.append("User name: " + u.name + "\n");
        }

        Toast.makeText(this, toastText.toString(), Toast.LENGTH_LONG).show();
    }

    /*
     * Testing methods:
     * Cursor queryWithFactory(SQLiteDatabase.CursorFactory cursorFactory, boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit, CancellationSignal cancellationSignal)
     * Cursor queryWithFactory(SQLiteDatabase.CursorFactory cursorFactory, boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
     */
    public void queryWithFactory(View view) {
        StringBuilder toastText = new StringBuilder();
        List<User> userList = sqLiteDatabaseHelper.queryWithFactory();

        for (User u : userList) {
            toastText.append("User name: " + u.name + "\n");
        }

        Toast.makeText(this, toastText.toString(), Toast.LENGTH_LONG).show();
    }

    /*
     * Testing methods:
     * Cursor rawQuery(String sql, String[] selectionArgs)
     * Cursor rawQuery(String sql, String[] selectionArgs, CancellationSignal cancellationSignal)
     */
    public void rawQuery(View view) {
        StringBuilder toastText = new StringBuilder();
        User user = sqLiteDatabaseHelper.getUser("user1");
        if (user != null) {
            toastText.append("Get user \n");
            toastText.append("User name: " + user.name + "\n");
        }
        List<User> userList = sqLiteDatabaseHelper.getAllUsers();

        toastText.append("Get users list\n");
        for (User u : userList) {
            toastText.append("User name: " + u.name + "\n");
        }

        Toast.makeText(this, toastText.toString(), Toast.LENGTH_LONG).show();
    }

    public void rawQueryWithFactory(View view) {
        StringBuilder toastText = new StringBuilder();
        List<User> userList = sqLiteDatabaseHelper.rawQueryWithFactory();
        for (User u : userList) {
            toastText.append("User name: " + u.name + "\n");
        }
        Toast.makeText(this, toastText.toString(), Toast.LENGTH_LONG).show();
    }

    /*
     * Testing methods:
     * long insert (String table, String nullColumnHack, ContentValues values)
     * long insertOrThrow (String table, String nullColumnHack, ContentValues values)
     * long insertWithOnConflict(String table, String nullColumnHack, ContentValues initialValues, int conflictAlgorithm)
     */
    public void insert(View view) {
        sqLiteDatabaseHelper.insert();
    }

    /*
     * Testing methods:
     * update(String table, ContentValues values, String whereClause, String[] whereArgs)
     * updateWithOnConflict(String table, ContentValues values, String whereClause, String[] whereArgs, int conflictAlgorithm)
     */
    public void update(View view) {
        sqLiteDatabaseHelper.updateUser();
    }

    // android.util.Log

    /*
     * Testing methods:
     * void d(String tag, String msg, Throwable tr)
     * void d(String tag, String msg)
     * void e(String tag, String msg, Throwable tr)
     * void e(String tag, String msg)
     * void i(String tag, String msg, Throwable tr)
     * void i(String tag, String msg)
     * void v(String tag, String msg, Throwable tr)
     * void v(String tag, String msg)
     * void w(String tag, String msg, Throwable tr)
     * void w(String tag, String msg)
     * void wtf(String tag, String msg, Throwable tr)
     * void wtf(String tag, String msg)
     */
    public void androidUtilLog(View view) {
        Log.d("DataStorageActivity", "Testing debug log");
        Log.d("DataStorageActivity", "Testing debug log", null);

        Log.e("DataStorageActivity", "Testing error log");
        Log.e("DataStorageActivity", "Testing error log", null);

        Log.i("DataStorageActivity", "Testing information log");
        Log.i("DataStorageActivity", "Testing information log", null);

        Log.v("DataStorageActivity", "Testing verbose log");
        Log.v("DataStorageActivity", "Testing verbose log", null);

        Log.w("DataStorageActivity", "Testing warning log");
        Log.w("DataStorageActivity", "Testing warning log", null);

        Log.wtf("DataStorageActivity", "Testing what a terrible failure log");
        Log.wtf("DataStorageActivity", "Testing what a terrible failure log", null);

    }

    /**
     * Testing methods:
     * void print(int i)
     * void print(double d)
     * void print(boolean b)
     * void print(char c)
     * void print(long l)
     * void print(float f)
     * void print(String s)
     * void print(Object obj)
     * void print(char[] s)
     * void println(int i)
     * void println(double d)
     * void println(boolean b)
     * void println(char c)
     * void println(long l)
     * void println(float f)
     * void println(String s)
     * void println(Object obj)
     * void println(char[] s)
     */
    public void printStream(View view) {
        PrintStream printStream = new PrintStream(System.out);
        printStream.print(1234566789);
        printStream.print(12.34);
        printStream.print(true);
        printStream.print('a');
        printStream.print((long) 12345);
        printStream.print(12.3456f);
        printStream.print("Testing PrintStream methods");
        printStream.print(new Object());
        printStream.print(new char[]{'a', 'b', 'c'});

        printStream.println(1234566789);
        printStream.println(12.34);
        printStream.println(true);
        printStream.println('a');
        printStream.println((long) 12345);
        printStream.println(12.3456f);
        printStream.println("Testing PrintStream methods");
        printStream.println(new Object());
        printStream.println(new char[]{'a', 'b', 'c'});

        printStream.flush();

    }

    /**
     * Testing methods:
     * void setInputType(int type)
     */
    public void setNoSuggestions(View view) {
        inputTypeTextView.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    /**
     * Testing methods:
     * void setCustomSelectionActionModeCallback(int type)
     */
    public void setCustomSelectionActionModeCallback(View view) {
        inputTypeTextView.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {

            }
        });
    }

    public void setLongClickable(View view) {
        inputTypeTextView.setLongClickable(false);
    }
}
