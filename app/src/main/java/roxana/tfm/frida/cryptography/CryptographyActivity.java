package roxana.tfm.frida.cryptography;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;

import roxana.tfm.frida.R;

public class CryptographyActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cryptography);
    }

    public void keyGeneratorInstance(View view) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            KeyGenerator.getInstance("AES", keyGenerator.getProvider().getName());
            KeyGenerator.getInstance("AES", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public void generateSymmetricKey(View view) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void keyPairGeneratorInstance(View view) {
        try {
            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("DSA");
            KeyPairGenerator.getInstance("DSA", keyGenerator.getProvider().getName());
            KeyPairGenerator.getInstance("DSA", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public void messageDigestInstance(View view) {
        try {
            MessageDigest keyGenerator = MessageDigest.getInstance("SHA-256");
            MessageDigest.getInstance("SHA-256", keyGenerator.getProvider().getName());
            MessageDigest.getInstance("SHA-256", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public void secretKeyFactoryInstance(View view) {
        try {
            SecretKeyFactory keyGenerator = SecretKeyFactory.getInstance("DES");
            SecretKeyFactory.getInstance("DES", keyGenerator.getProvider().getName());
            SecretKeyFactory.getInstance("DES", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public void signatureInstance(View view) {
        try {
            Signature keyGenerator = Signature.getInstance("DSA");
            Signature.getInstance("DSA", keyGenerator.getProvider().getName());
            Signature.getInstance("DSA", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public void cipherInstance(View view) {
        try {
            Cipher keyGenerator = Cipher.getInstance("DES/CBC/PKCS5Padding");
            Cipher.getInstance("DES/CBC/PKCS5Padding", keyGenerator.getProvider().getName());
            Cipher.getInstance("DES/CBC/PKCS5Padding", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public void macInstance(View view) {
        try {
            Mac keyGenerator = Mac.getInstance("HmacSHA256");
            Mac.getInstance("HmacSHA256", keyGenerator.getProvider().getName());
            Mac.getInstance("HmacSHA256", keyGenerator.getProvider());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    public void getRandomValues(View view) {
        Random random = new Random();
        random.nextInt();
        random.nextInt(10);
        random.nextDouble();
        random.nextBoolean();
        random.nextFloat();
        random.nextLong();
        random.nextGaussian();
    }
}
